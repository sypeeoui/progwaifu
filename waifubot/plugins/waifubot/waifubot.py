import os,time,random,json
from chatterbot import ChatBot
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
lista=["G-grazie per avermi usata :)", "S-so di non essere molto brava...t-ti prego però, n-on abbandonarmi anche tu...", "G-grazie, ho imparato molte cose oggi M-Master...", "Vuoi mangiare? Vuoi fare un bagno? Oppure vuoi...me? ;p", "Il mio programmatore spera che tu ti sia divertito :3"]
def inverso_lookup(d, value):
	return random.choice([k for k, v in d.items() if v==value])
	raise LookupError("Valore inesistente.")
#parte per il bot telegram
wait=0
#Sessioni attive idchat:sessioneAI
AI_s={}
def new_session(c_id,personalità,**a):
	global AI_s
	os.chdir("personalità/"+personalità)
	if "read_only" in a:
		read_only=a["read_only"]
	else:
		read_only=False
	chatbot = ChatBot(
		personalità,
		storage_adapter='chatterbot.storage.SQLStorageAdapter',
		read_only=read_only
    )
	AI_s[c_id]=chatbot
	os.chdir("..")
	os.chdir("..")
	return True
def pensa(chatbot,user_input):
    bot_response = chatbot.get_response(user_input)
    return bot_response

from pyrogram import filters,Client
#list di parole chiave con cui il bot di sentirà taggato
taglist=["@waifu_waifu_bot","waifubot","waifu"]
#me
notme=True
me=None
@Client.on_message(filters.text,group=3)
async def waifuchat(client,message):
	global notme,me,AI_s
	m_id=message.message_id
	c_id=message.chat.id
	if notme:
		me=await client.get_me()
		notme=False
	#controllo stupido per vedere se il bot è stato taggato
	try:
		tag=message.reply_to_message.from_user.id==me.id
	except:
		tag=False
	if message.text==None:
		return
	if not tag:
		for i in taglist:
			tag=tag or i in message.text.lower().split(" ")
	if c_id in AI_s and (tag or c_id>0):
		#attesa di un tempo random
		time.sleep(random.randint(5,20)/10)
		await client.send_message(c_id,str(pensa(AI_s[c_id],message.text.lower())))
#start
@Client.on_message(filters.command(["start","start@waifu_waifu_bot"]),group=2)
async def startchat(client,message):
	m_id=message.message_id
	c_id=message.chat.id
	await client.send_message(c_id,'''Questo programma è stato realizzato da Berry Uzumaki nella speranza di potere, un giorno, trovare la Waifu perfetta.
		Il programma è ancora incompleto ed imperfetto, quindi se trovi dei bug oppure hai delle idee innovative, inoltra tutto alla mail playerberryuzumaki@gmail.com.
		A proposito, le personalità della Waifu per ora realizzate sono "dandere, tsundere, yandere, kuudere". Ne aggiungerò altre in futuro.''')
	time.sleep(1)
	await client.send_message(c_id,"Se non sai come funzioni il programma, digita /help.")
#help
@Client.on_message(filters.command(["help","help@waifu_waifu_bot"]),group=4)
async def helpchat(client,message):
	m_id=message.message_id
	c_id=message.chat.id
	await client.send_message(c_id,'''Una piccola introduzione ai comandi di questo programma:
		/set: per impostare la personalità. Sono disponibili "tsundere,dandere,tsundere,kuudere"
		/stop: da utilizzare quando si sono concluse tutte le operazioni che si volevano eseguire, permette anche il salvaggio delle nuove interazioni. Non uscire dal programma senza usare questo comando, altrimenti al prossimo avvio del programma le ultime frasi da te inserite non saranno presenti.
		/help: ti verrà scritto questo testo, quindi usalo nel caso in cui ti venisse qualche dubbio sull'utilizzo dei comandi.
		Spero che la Waifu sia di tuo gradimento! ;-)''')
@Client.on_message(filters.command(["stop","stop@waifu_waifu_bot"]),group=6)
async def stop(client,message):
	global AI_s,lista
	c_id=message.chat.id
	await client.send_message(c_id,random.choice(lista))
	del AI_s[c_id]
@Client.on_message(filters.command(["set","set@waifu_waifu_bot"]),group=8)
async def set_personalità(client,message):
	m_id=message.message_id
	c_id=message.chat.id
	personalità=message.text.split(" ",1)[1].lower()
	try:
		new_session(c_id,personalità)
		await client.send_message(c_id,"Personalità impostata!")
	except Exception as e:
			print(e)
			await client.send_message(c_id,"C'è stato qualche errore... o la personalità non esiste")
@Client.on_message(filters.command(["train","train@waifu_waifu_bot"]),group=7)
async def train_personalità(client,message):
	m_id=message.message_id
	c_id=message.chat.id
	personalità=message.text.split(" ",1)[1].lower()
	try:
		new_session(c_id,personalità,read_only=True)
		await client.send_message(c_id,"Sono lieta di imparare nuove cose da te! Master. . .")
	except:
			await client.send_message(c_id,"C'è stato qualche errore... o la personalità non è stata trovata")
